var express = require("express");
var router = express.Router();
const checkUserProperties = require("../utils/checkUserProperties");
const model = require("../models");

/* GET users listing. */
router.get("/", async (req, res) => {
  try {
    const users = await model.Users.findAll();
    return res.send({ users });
  } catch (error) {
    res.status(400).send({
      message: "an error has occured, could not retrieve users",
      error
    });
  }
});

router.post("/", async (req, res, next) => {
  try {
    if (!checkUserProperties(req.body)) {
      throw new Error("Missing or invalid property");
    }
    const newUser = await model.Users.create({
      ...req.body,
      createdDate: `${new Date()}`
    });
    return res.status(201).send(newUser);
  } catch (error) {
    console.log("error: ", error.message);
    res.status(400).send({
      message: "An error has occured, could not create user",
      error: error.message
    });
  }
});

router.get("/:id", async (req, res, next) => {
  try {
    const userById = await model.Users.findByPk(req.params.id);
    return res.send({ user: userById });
  } catch (error) {
    res.status(400).send({
      message: "an error has occured, could not retrieve user",
      error
    });
  }
});

router.put("/:id", async (req, res, next) => {
  res.send({ id: req.params.id });
  try {
    const updatedUser = await model.Users.update(req.body, {
      where: { id: req.params.id }
    });
    return res.status(200).send({ user: updatedUser });
  } catch (error) {}
});

module.exports = router;
