function checkUserProperty(user) {
  // Check that there is no missing needed property
  if (!user || !user.firstName || !user.lastName || !user.email) {
    return false;
  }

  // Check that every property is a string
  return Object.keys(user).every(
    propertyName => typeof user[propertyName] === "string"
  );
}

module.exports = checkUserProperty;
