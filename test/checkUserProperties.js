const assert = require("assert");
const checkUserProperties = require("../utils/checkUserProperties");

describe("Validate user properties presence and existance", function() {
  it("should return false if one or many properties are missing", function() {
    const mockUser = {
      firstName: "test",
      lastName: "test"
    };
    assert.equal(checkUserProperties(mockUser), false);
  });
});
